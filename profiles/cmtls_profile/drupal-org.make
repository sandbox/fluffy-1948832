; Core
core = 7.x

api = 2

; Modules

projects[advanced_help][version]  = "1.0"

projects[admin_menu][version]  = "3.0-rc2"

projects[ajax_comments][version] = "1.0-alpha1"

projects[ctools][version] = "1.0"

projects[devel][version] = "1.2"

projects[context][version] = "3.0-beta4"

projects[entity][version] = "1.0-rc2"

projects[google_analytics][version] = "1.2"

projects[references][version] = "2.0"

projects[og][version] = "1.5"

projects[og_node_link][version] = "1.0"

projects[og_views][version] = "1.x-dev"

projects[pathauto][version] = "1.0"

projects[panels][version] = "3.2"

projects[cache_actions][version] = "2.0-alpha4"

projects[rules][version] = "2.2"
projects[rules][patch][] = "http://drupal.org/files/rules-schema-fix-1952170-1.patch"
projects[rules][patch][] = "http://drupal.org/files/1547160-9-rules-variables-direct-input.patch"
projects[rules][patch][] = "http://drupal.org/files/rules-token-evaluator-handles-empty-values-1559844-2c.patch"

projects[realname][version] = "1.0-rc2"

projects[token][version] = "1.0"

projects[transliteration][version] = "3.0"

projects[redirect][version] = "1.0-beta4"

projects[views][version] = "3.5"

projects[webform][version] = "3.17"

projects[wysiwyg][version] = "2.1"

projects[media][version] = "1.0"

projects[media_youtube][version] = "1.0-beta3"

projects[media_vimeo][version] = "1.0-beta5"

projects[ds][version] = "1.5"

projects[fb][version] = "3.3-beta5"
projects[fb][patch][] = "http://drupal.org/files/fb_captcha-1525116-5.patch"

projects[module_filter][version] = "1.6"

projects[features][version] = "1.0-rc2"

projects[ftools][version] = "1.4"

projects[field_group][version] = "1.1"

projects[diff][version] = "2.0"

projects[logintoboggan][version] = "1.3"

projects[delta][version] = "3.x-dev"

projects[i18n][version] = "1.5"

projects[variable][version] = "1.2"

projects[draggableviews][version] = "2.0-beta1"

projects[nice_menus][version] = "2.1"

projects[flag][version] = "2.0-beta7"

projects[views_bulk_operations][version] = "3.0-rc1"

projects[captcha][version] = "1.0-beta2"

projects[hidden_captcha][version] = "1.0"

projects[openlayers][version] = "2.0-beta3"
projects[openlayers][patch][] = "http://drupal.org/files/openlayers_views-block-visibility-768294-29.patch"

projects[addressfield][version] = "1.0-beta3"

projects[geofield][version] = "1.1"

projects[geophp][version] = "1.7"

projects[timeago][version] = "2.1"
projects[timeago][patch][] = "http://drupal.org/files/timeago-wrong-file-include-order-1832550-4.patch"

projects[libraries][version] = "2.0"

; Themes
projects[omega][version] = "3.1"

; Libraries
libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.2/ckeditor_3.6.2.tar.gz"

; This is not whitelisted
;libraries[facebook-php-sdk][download][type] = "file"
;libraries[facebook-php-sdk][download][url] = "https://github.com/facebook/facebook-php-sdk/archive/v3.1.1.tar.gz"

libraries[timeago][download][type] = "file"
libraries[timeago][download][url] = "http://timeago.yarp.com/jquery.timeago.js"

; Disable Drupal Core profiles
;projects[drupal][patch][drupal.disable-profiles.patch] = http://drupal.org/files/spark-install-1780598-5.patch

projects[calendar][version] = "3.2"

projects[date][version] = "2.5"

projects[media_browser_plus][version] = "1.0-beta3"

projects[media_gallery][version] = "1.0-beta8"
projects[media_gallery][patch][] = "http://drupal.org/files/media_gallery-all_files_download-1817552-1.patch"

projects[multiform][version] = "1.0"

projects[plupload][version] = "1.0-rc1"

libraries[colorbox][download][type] = "file"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/zipball/v1.3.16"

libraries[plupload][download][type] = "file"
libraries[plupload][download][url] = "https://github.com/downloads/moxiecode/plupload/plupload_1_5_1_1_dev.zip"
